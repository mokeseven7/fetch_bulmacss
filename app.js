//Listen for enter keypress on form
document.querySelector('#zipForm').addEventListener('submit', getLocationInfo);

//listen for delete:
document.querySelector('body').addEventListener('click', deleteLocation);


//Callback for form submit
function getLocationInfo(event){
    //dont submit the form
    event.preventDefault();
    //Get zip value from input
    const zip = document.querySelector('.zip').value;
    //Call the ZIPCODE api with fetch
    fetch(`http://api.zippopotam.us/us/${zip}`)
    .then((res) => {
        if(res.status !== 200){
            badZip();
            showIcon('remove');
            throw Error(res.statusText);
        }else{
            showIcon('check');
            console.log()
            return res.json();
        }
    })
    .then((data) => {
        //Show Location info
        let output = '';
        data.places.forEach((place) => {
            output += `
                <article class="message is-primary">
                    <div class="message-header">
                        <p>Location Info:</p>
                        <button class="delete"></div>
                    </div>
                    <div class="message-body">
                        <ul>
                        <li><strong>City: </strong>${place['place name']}</li>
                        <li><strong>State: </strong>${place['state']}</li>
                        <li><strong>Longitude: </strong>${place['longitude']}</li>
                        <li><strong>Latitude: </strong>${place['latitude']}</li>
                        </ul>
                    </div>
                </article>
            
            `;
        });
        //insert output into dom
        document.querySelector('#output').innerHTML = output;
    })
    .catch((err) => console.log(err))
}

//User has entered a bad zipcode
function badZip(){
    //Main Paretnt to append everything to
    let output = document.querySelector('#output');
    //Clear out current error
    output.innerHTML = '';
    //Create the article
    let article = document.createElement('article');
    //Set classes on the article
    article.setAttribute('class', 'message is-danger');
    let messageBody = document.createElement('div');
    //set the classes on the message body div
    messageBody.setAttribute('class', 'message-body');
    //set the erorr message
    messageBody.textContent = `Sorry, That is not a valid zip code`;
    //append the message body to the parent
    article.appendChild(messageBody);
    //Append the error parent to the dom
    output.appendChild(article);
    
}


//This function decides which icon to show on the left side of the input..
//Based on weather there is an error when calling the API
function showIcon(icon){
    //clear icons
    document.querySelector('.icon-remove').style.display = 'none';
    document.querySelector('.icon-check').style.display = 'none';
    //Show the correct icon
    document.querySelector(`.icon-${icon}`).style.display = 'inline-flex';
}

//Delete button was clicked
function deleteLocation(event){
    if(event.target.className == 'delete'){
        document.querySelector('.message').remove();
        document.querySelector('.zip').value = '';
        document.querySelector('.icon-check').style.display = 'none';
    }
}